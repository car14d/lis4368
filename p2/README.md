> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Cameron Rapp

### Project #2 Requirements:

*Four Parts:*

1. Clone student files
2. Modify files to match
3. Write .java files accordingly
4. Chapter Questions (16 - 17)

#### README.md file should include the following items:

* Screenshot of valid user form entry
* Screenshot of successful validation
* Screenshot of display database
* Screenshot of modify form
* Screenshot of modified data
* Screenshot of delete warning
* Screenshot of database





#### Assignment Screenshots:



*Screenshot of data entry validation*:

![Entry Screenshot](img/p2.png)

*Screenshot of successful validation*:

![Success Screenshot](img/thanks.png)

*Screenshot of display database *:

![Display Database](img/customer.png)

*Screenshot of modify form*:

![Modify form](img/update.png)

*Screenshot of modified data*:

![Modified Data](img/changedcustomer.png)

*Screenshot of delete warning*:

![Delete Warning](img/delete.png)

*Screenshot of database*:

![Database](img/databasechange.png)





