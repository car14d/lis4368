> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Cameron Rapp

### Assignment #2 Requirements:

*Sub-Heading:*

1. Download and install MySQL
2. Develop and Deploy a WebApp
3. Deploying Servlet using @WebServlet

#### README.md file should include the following items:

* Assessment links
* Only *one* screenshot of the query results from the following link: http://localhost:9999/hello/querybook.html
* [http://localhost:9999/hello](http://localhost:9999/hello) 
* [http://localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/HelloHome.html)
(Rename "HelloHome.html" to "index.html")
* [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello) (invokes HelloServlet)
Note: /sayhello maps to HelloServlet.class 
* [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)
* [http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi) 



#### Assignment Screenshots:

*Screenshot of query results*:

![Querybook.html Screenshot](img/helloquery.png)




