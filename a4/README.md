> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Cameron Rapp

### Assignment #4 Requirements:

*Three Parts:*

1. Clone student files
2. Modify files to match
3. Chapter Questions (11,12)

#### README.md file should include the following items:

* Screenshot of successful validation
* Screenshot of failed validation




#### Assignment Screenshots:



*Screenshot of successful validation*:

![Success Screenshot](img/successful.png)

*Screenshot of failed validation*:

![Failed Screenshot](img/failed.png)



