> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Cameron Rapp

### Assignment #5 Requirements:

*Three Parts:*

1. Clone student files
2. Modify files to match
3. Write .java files accordingly
4. Chapter Questions (13 - 15)

#### README.md file should include the following items:

* Screenshot of data entry
* Screenshot of successful validation
* Screenshot of database entry




#### Assignment Screenshots:



*Screenshot of data entry validation*:

![Entry Screenshot](img/dataentry.png)

*Screenshot of successful validation*:

![Success Screenshot](img/datavalidation.png)

*Screenshot of database before *:

![Before Database Screenshot](img/databasebefore.png)

*Screenshot of database after*:

![After Database Screenshot](img/databaseafter.png)






