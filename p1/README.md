> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Cameron Rapp

### Project #1 Requirements:

*Three Parts:*

1. Clone student files
2. Modify index.jsp file
3. Validate data
4. Chapter Questions (9,10)

#### README.md file should include the following items:




#### Assignment Screenshots:



*Screenshot of Online Portfolio*:

![Online Portfolio Screenshot](img/onlineportfolio.png)

*Screenshot of Failed Validation*:

![Fail Screenshot](img/failedvalidation.png)

*Screenshot of Successful Validation*:

![Success Screenshot](img/successfulvalidation.png)


#### Local host link:
[localhost:9999](http://localhost:9999/LIS4368/)






