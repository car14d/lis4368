SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `car14d` ;
CREATE SCHEMA IF NOT EXISTS `car14d` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
SHOW WARNINGS;
USE `car14d` ;

-- -----------------------------------------------------
-- Table `car14d`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `car14d`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `car14d`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) NOT NULL,
  `pst_phone` BIGINT NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `car14d`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `car14d`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `car14d`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT NOT NULL,
  `cus_phone` BIGINT NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `car14d`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `car14d`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `car14d`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m','f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NOT NULL,
  `pet_vaccine` ENUM('y','n') NOT NULL,
  `pet_neuter` ENUM('y','n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `car14d`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `car14d`.`customer` (`cus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `car14d`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `car14d`;
INSERT INTO `car14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (1, 'PetSmart', '4885 Whispering Pine Way', 'Naples', 'FL', 287734103, 2398797978, 'petsmartnaples@gmail.com', 'petsmart.com', 930000.00, 'smart pets');
INSERT INTO `car14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (5, 'PetsMart', '8225 Vera Cruz Way', 'Naples', 'FL', 265534109, 2390980883, 'betterpetsmartnaples@gmail.com', 'petsmartmart.com', 780000.00, 'pet mart');
INSERT INTO `car14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (3, 'Petco', '6849 Il Regalo Circle', 'Naples', 'FL', 265534109, 2393298472, 'petcoisbetteranyway@yahoo.com', 'petco.com', 999999.99, 'different from petsmart');
INSERT INTO `car14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (7, 'Ain\'t Nuthin\' But Pets', '4555 That Way', 'Atlanta', 'GA', 167832304, 4042832922, 'aintnuthin@comcast.net', 'ainnuthinbutpets.com', 232003.76, NULL);
INSERT INTO `car14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (9, 'Whole Lotta Pets', '533 That Street', 'Cincinatti', 'OH', 822273287, 1234344123, 'ledzeppfan@gmail.com', 'wholelottapets.com', 254033.33, 'look for the ole mill');
INSERT INTO `car14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (2, 'Pet On It', '8000 Way Drive', 'Las Vegas', 'NV', 129832562, 7347854858, 'solidpun@gmail.com', 'petonit.com', 123333.00, 'down 5th ave');
INSERT INTO `car14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (6, 'You Pet Believe It', '221 Father Road', 'Shanghai', 'FL', 489029033, 2390989381, 'betterpun@hotmail.com', 'youpetbelieveit.com', 537288.00, NULL);
INSERT INTO `car14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (4, 'Petter Late than Never', '3222 Triggered Way', 'Charleston', 'SC', 231354466, 3136767655, 'bestpun@gmail.com', 'petterlatethannever.com', 355667.32, 'south carolina');
INSERT INTO `car14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (8, 'Pet', '666 Canyon Blvd', 'Wallabee', 'AZ', 465334244, 2910264530, 'okaynowyourejustbeinglazy@gmail.com', 'petstorewallabee.com', 123123.99, NULL);
INSERT INTO `car14d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (10, 'Oh Boy, Pets.', '1100 Way Out Drive', 'Scranton', 'OH', 243445535, 0092233828, 'okwellalright@yahoo.com', 'ohboyzzz.com', 313123.98, 'ok well alright');

COMMIT;


-- -----------------------------------------------------
-- Data for table `car14d`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `car14d`;
INSERT INTO `car14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (1, 'Cameron', 'Rapp', '201 N Lipona Rd', 'Tallahassee', 'FL', 778832304, 2395807256, 'car14d@my.fsu.edu', 2233.98, 109.88, 'it\'s me');
INSERT INTO `car14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (3, 'Johnnyman', 'Dontdieonme', '2333 Greaser Street', 'New York', 'NY', 312303109, 2316849968, 'mattdillon@helpmycareer.com', 00.96, 00.00, 'really matt dillon');
INSERT INTO `car14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (6, 'Jonatan', 'Leandor', '8233 Hoover Street', 'Yoshi City', 'CA', 382416033, 1486931462, 'sadboys@gmail.com', 345000.73, 123.98, '2001');
INSERT INTO `car14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (2, 'Sean', 'Carter', '956 Jigga Blvd.', 'Brooklyn', 'NY', 129387498, 1693287463, 'hova@yahoo.com', 78000.76, 670.90, 'runs new york');
INSERT INTO `car14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (9, 'Ghostface', 'Killah', '1888 Ironman Circle', 'Queens', 'NY', 814326403, 1691423890, 'supremeclientelle@wutang.com', 34088.22, 123.22, NULL);
INSERT INTO `car14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (7, 'Donald', 'Glover', '7366 Kauai Road', 'Stone Mountain', 'GA', 123332334, 4041249363, 'childishgambino@gmail.com', 120.00, 80.00, 'real email');
INSERT INTO `car14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (4, 'Han', 'Solo', '9988 Cantina Road', 'Bespin', 'AZ', 321431244, 4120873327, 'scruffylookingnerfherder@spaceemail.com', 00.01, 2300.32, NULL);
INSERT INTO `car14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (8, 'Kodak', 'Black', '223 Sniper Corner', 'Miami', 'FL', 287094873, 3054328947, 'noloveydovey@snipergang.com', 100.00, 100.00, 'he\'s the boss of that');
INSERT INTO `car14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (5, 'Rick', 'Sanchez', '1233 Wubba Dub Terrace', 'Ocala', 'FL', 528074832, 7984374222, 'riggetywreckedson@gmail.com', 00.50, 00.00, NULL);
INSERT INTO `car14d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (10, 'Morty', 'Smith', '1233 Wubba Dub Terrace', 'Ocala', 'FL', 528074832, 7981345324, 'ohjeezrick@yahoo.com', 00.00, 00.00, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `car14d`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `car14d`;
INSERT INTO `car14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 10, 1, 'Dog', 'm', 130.00, 180.00, 1, 'brown', '2016-05-25', 'y', 'y', 'shih-tzu');
INSERT INTO `car14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 2, 3, 'Cat', 'f', 00.30, 170.00, 6, 'yellow', '2010-06-22', 'y', 'n', 'demon');
INSERT INTO `car14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 6, 10, 'Mosquito', 'm', 2000.00, 3000.00, 35, 'black', '2009-04-02', 'n', 'y', 'will suck blood');
INSERT INTO `car14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 7, 4, 'Small Spider', 'm', 20.00, 30.00, 1, 'black', '2015-09-12', 'n', 'y', NULL);
INSERT INTO `car14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 9, 5, 'Banana Squirrel', 'm', 85.00, 100.00, 4, 'yellow', '2016-12-13', 'y', 'n', 'loves oranges');
INSERT INTO `car14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 1, 7, 'Obscure Mediterranean Mongoose', 'f', 100000.00, 150000.00, 3, 'tan', '2014-08-22', 'n', 'y', 'very rare');
INSERT INTO `car14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 5, 9, 'A Frog', 'm', 15.99, 20.00, 1, 'green', '2012-09-26', 'y', 'n', NULL);
INSERT INTO `car14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 3, 2, 'A Different Frog', 'f', 16.00, 20.00, 2, 'blue', '2016-07-31', 'y', 'n', 'this one is blue');
INSERT INTO `car14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 8, 8, 'Goldfish', 'f', 2.00, 5.00, 67, 'orange', '2011-03-17', 'n', 'y', NULL);
INSERT INTO `car14d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 4, 6, 'A Third Frog', 'f', 16.01, 20.00, 3, 'green', '2012-05-21', 'n', 'y', 'similar to frog one');

COMMIT;

