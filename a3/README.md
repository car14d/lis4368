> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Cameron Rapp

### Assignment #3 Requirements:

*Three Parts:*

1. Create database with specified requirements
2. Forward engineer
3. Chapter Questions (7,8)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of ERD;
* Links to the following files:
  a. a3.mwb
  b. a3.sql


#### Assignment Screenshots:



*Screenshot of ERD*:

![ERD Screenshot](img/a3.png)



#### File Links:

*MWB:*
[MWB](a3.mwb)
*SQL:*
[SQL](a3.sql)
