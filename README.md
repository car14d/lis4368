> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368

## Cameron Rapp

### Assignment Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Create Bitbucket account and repo
    - Provide Installation screenshots
    - Complete Bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MYSQL
    - Develop and deploy WebApp
    - Use WebServlet to deploy WebApp
    - Provide specified screenshots
    - Answer questions
    
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Install Workbench if not done previously
    - Create ERD with required specifications
    - Forward engineer
    - Provide specified screenshots
    - Answer questions

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Clone student files
    - Modify index.jsp file accordingly
    - Validify data in the program
    - Answer questions

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Clone student files
    - Modify files accordingly
    - Validify data in the program
    - Answer questions

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Clone student files
    - Modify files accordingly
    - Write data .java files
    - Validify data in the program
    - Answer questions

6. [P2 README.md](p2/README.md "My P2 README.md file")
    - Clone student files
    - Modify files accordingly
    - Write data .java files
    - Validify data in the program
    - Answer questions